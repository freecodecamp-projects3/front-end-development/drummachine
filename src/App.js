import "./App.css";

class DrumMachine extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      nowPlaying: "",
    };
    this.setNowPlaying = this.setNowPlaying.bind(this);
    this.keypressHandler = this.keypressHandler.bind(this);
  }
  componentDidMount() {
    document.addEventListener("keypress", this.keypressHandler);
  }
  componentWillUnmount() {
    document.removeEventListener("keypress", this.keypressHandler);
  }
  playSound(key) {
    const audioTag = document.getElementById(`${key}`);
    audioTag.play();
  }
  setNowPlaying(soundDescription) {
    this.setState({ nowPlaying: soundDescription });
  }
  keypressHandler(event) {
    const key = event.key.toUpperCase();
    const drumpad = drumPads.filter((drumpad) => drumpad.key === key)[0];
    if (drumpad) {
      this.setNowPlaying(drumpad.soundDescription);
      this.playSound(drumpad.key);
    }
  }
  render() {
    return (
      <div id="drum-machine">
        <div id="display">{this.state.nowPlaying}</div>
        {drumPads.map((drumpad, index) => {
          return (
            <Drumpad
              key={index}
              drumpad={{
                ...drumpad,
                setNowPlaying: this.setNowPlaying,
                playSound: this.playSound,
              }}
            />
          );
        })}
      </div>
    );
  }
}
class Drumpad extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      key: props.drumpad.key,
      audioSrc: props.drumpad.audioSrc,
      soundDescription: props.drumpad.soundDescription,
      setNowPlaying: props.drumpad.setNowPlaying,
      playSound: props.drumpad.playSound,
    };
  }

  render() {
    return (
      <div
        className="drum-pad"
        id={this.state.soundDescription}
        onClick={(event) => {
          this.state.setNowPlaying(this.state.soundDescription);
          this.state.playSound(this.state.key);
        }}
      >
        {this.state.key}
        <audio
          id={this.state.key}
          src={this.state.audioSrc}
          className="clip"
        ></audio>
      </div>
    );
  }
}
const drumPads = [
  {
    key: "Q",
    audioSrc: "https://s3.amazonaws.com/freecodecamp/drums/Heater-1.mp3",
    soundDescription: "Heater-1",
  },
  {
    key: "W",
    audioSrc: "https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3",
    soundDescription: "Heater-2",
  },
  {
    key: "E",
    audioSrc: "https://s3.amazonaws.com/freecodecamp/drums/Heater-3.mp3",
    soundDescription: "Heater-3",
  },
  {
    key: "A",
    audioSrc: "https://s3.amazonaws.com/freecodecamp/drums/Heater-4_1.mp3",
    soundDescription: "Heater-4",
  },
  {
    key: "S",
    audioSrc: "https://s3.amazonaws.com/freecodecamp/drums/Heater-6.mp3",
    soundDescription: "Clap",
  },
  {
    key: "D",
    audioSrc: "https://s3.amazonaws.com/freecodecamp/drums/Dsc_Oh.mp3",
    soundDescription: "Open-HH",
  },
  {
    key: "Z",
    audioSrc: "https://s3.amazonaws.com/freecodecamp/drums/Kick_n_Hat.mp3",
    soundDescription: `Kick-n'-Hat`,
  },
  {
    key: "X",
    audioSrc: "https://s3.amazonaws.com/freecodecamp/drums/RP4_KICK_1.mp3",
    soundDescription: "Kick",
  },
  {
    key: "C",
    audioSrc: "https://s3.amazonaws.com/freecodecamp/drums/Cev_H2.mp3",
    soundDescription: `Closed-HH`,
  },
];

export default DrumMachine;
